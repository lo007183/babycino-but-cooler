// Bug H3. End the loop when the condition is true.

class TestBugH3 {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {

    public int f() {
	int result;
	int count;
	boolean done;
	result = 0;
	count = 1;
	done = true;
	do {
	    result = result + count;
	    done = (10 < result); 
	} while (!done);
    // Result should be 11 when this stops. If not, bug is present.
	return result;
    }

}