// Bug H1. Allow an integer as the condition. Copy of TestFeatureH to implement bug.

class TestBugH1 {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {

    public int f() {
	int result;
	int count;
	boolean done;
    int number;
	result = 0;
	count = 1;
	done = true;
	do {
	    result = result + count;
	    count = count + 1;
	    done = (10 < count);
	} while (13); // use the number here. If bug is present this will be valid and compile.
	return result;
    }

}