// Bug H2. Check the condition at the start of the loop, not the end.

class TestBugH2 {
    public static void main(String[] a) {
	System.out.println(new Test().f());
    }
}

class Test {

    public int f() {
	int result;
	int count;
	boolean done;
	result = 0;
	count = 1;
	done = false; // When done is false, the loop will exit.
	do {
	    result = result + count;
	} while (done); // If the loop is not implemented correctly the result will be 0.
	return result; // Return the result. If 1, good, if 0, bad.
    }

}